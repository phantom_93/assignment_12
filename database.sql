USE [master]
GO
/****** Object:  Database [PostGrad]    Script Date: 8/31/2020 10:56:38 AM ******/
CREATE DATABASE [PostGrad]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'PostGrad', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\PostGrad.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'PostGrad_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\PostGrad_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [PostGrad] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [PostGrad].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [PostGrad] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [PostGrad] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [PostGrad] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [PostGrad] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [PostGrad] SET ARITHABORT OFF 
GO
ALTER DATABASE [PostGrad] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [PostGrad] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [PostGrad] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [PostGrad] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [PostGrad] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [PostGrad] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [PostGrad] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [PostGrad] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [PostGrad] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [PostGrad] SET  ENABLE_BROKER 
GO
ALTER DATABASE [PostGrad] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [PostGrad] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [PostGrad] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [PostGrad] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [PostGrad] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [PostGrad] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [PostGrad] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [PostGrad] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [PostGrad] SET  MULTI_USER 
GO
ALTER DATABASE [PostGrad] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [PostGrad] SET DB_CHAINING OFF 
GO
ALTER DATABASE [PostGrad] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [PostGrad] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [PostGrad] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [PostGrad] SET QUERY_STORE = OFF
GO
USE [PostGrad]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 8/31/2020 10:56:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Professors]    Script Date: 8/31/2020 10:56:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Professors](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](max) NULL,
	[LastName] [nvarchar](max) NULL,
	[DOB] [datetime2](7) NOT NULL,
	[Gender] [int] NOT NULL,
 CONSTRAINT [PK_Professors] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProfessorStudents]    Script Date: 8/31/2020 10:56:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProfessorStudents](
	[ProfessorId] [int] NOT NULL,
	[StudentId] [int] NOT NULL,
 CONSTRAINT [PK_ProfessorStudents] PRIMARY KEY CLUSTERED 
(
	[ProfessorId] ASC,
	[StudentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Students]    Script Date: 8/31/2020 10:56:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Students](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](max) NULL,
	[LastName] [nvarchar](max) NULL,
	[DOB] [datetime2](7) NOT NULL,
	[Gender] [int] NOT NULL,
 CONSTRAINT [PK_Students] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200831083139_PostGradDB', N'3.1.7')
GO
SET IDENTITY_INSERT [dbo].[Professors] ON 

INSERT [dbo].[Professors] ([Id], [FirstName], [LastName], [DOB], [Gender]) VALUES (1, N'Nicholas', N'Warrior', CAST(N'1992-03-27T00:00:00.0000000' AS DateTime2), 0)
INSERT [dbo].[Professors] ([Id], [FirstName], [LastName], [DOB], [Gender]) VALUES (2, N'Dewald', N'Els', CAST(N'1992-03-27T00:00:00.0000000' AS DateTime2), 0)
INSERT [dbo].[Professors] ([Id], [FirstName], [LastName], [DOB], [Gender]) VALUES (4, N'amalie', N'steward', CAST(N'1992-08-08T00:00:00.0000000' AS DateTime2), 1)
SET IDENTITY_INSERT [dbo].[Professors] OFF
GO
INSERT [dbo].[ProfessorStudents] ([ProfessorId], [StudentId]) VALUES (1, 1)
INSERT [dbo].[ProfessorStudents] ([ProfessorId], [StudentId]) VALUES (1, 2)
INSERT [dbo].[ProfessorStudents] ([ProfessorId], [StudentId]) VALUES (1, 3)
INSERT [dbo].[ProfessorStudents] ([ProfessorId], [StudentId]) VALUES (2, 4)
INSERT [dbo].[ProfessorStudents] ([ProfessorId], [StudentId]) VALUES (2, 5)
INSERT [dbo].[ProfessorStudents] ([ProfessorId], [StudentId]) VALUES (2, 6)
GO
SET IDENTITY_INSERT [dbo].[Students] ON 

INSERT [dbo].[Students] ([Id], [FirstName], [LastName], [DOB], [Gender]) VALUES (1, N'Anna', N'Banana', CAST(N'1993-03-27T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Students] ([Id], [FirstName], [LastName], [DOB], [Gender]) VALUES (2, N'Andre', N'Ottosen', CAST(N'1992-03-27T00:00:00.0000000' AS DateTime2), 0)
INSERT [dbo].[Students] ([Id], [FirstName], [LastName], [DOB], [Gender]) VALUES (3, N'Mathias', N'Brentsen', CAST(N'1996-12-27T00:00:00.0000000' AS DateTime2), 0)
INSERT [dbo].[Students] ([Id], [FirstName], [LastName], [DOB], [Gender]) VALUES (4, N'Emperor', N'Anderson', CAST(N'1996-03-27T00:00:00.0000000' AS DateTime2), 0)
INSERT [dbo].[Students] ([Id], [FirstName], [LastName], [DOB], [Gender]) VALUES (5, N'Katharin', N'Potato', CAST(N'1996-03-27T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Students] ([Id], [FirstName], [LastName], [DOB], [Gender]) VALUES (6, N'Natalaya', N'Stina', CAST(N'1996-03-27T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Students] ([Id], [FirstName], [LastName], [DOB], [Gender]) VALUES (8, N'hanna', N'hanson', CAST(N'1997-09-09T00:00:00.0000000' AS DateTime2), 1)
SET IDENTITY_INSERT [dbo].[Students] OFF
GO
/****** Object:  Index [IX_ProfessorStudents_StudentId]    Script Date: 8/31/2020 10:56:38 AM ******/
CREATE NONCLUSTERED INDEX [IX_ProfessorStudents_StudentId] ON [dbo].[ProfessorStudents]
(
	[StudentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ProfessorStudents]  WITH CHECK ADD  CONSTRAINT [FK_ProfessorStudents_Professors_ProfessorId] FOREIGN KEY([ProfessorId])
REFERENCES [dbo].[Professors] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ProfessorStudents] CHECK CONSTRAINT [FK_ProfessorStudents_Professors_ProfessorId]
GO
ALTER TABLE [dbo].[ProfessorStudents]  WITH CHECK ADD  CONSTRAINT [FK_ProfessorStudents_Students_StudentId] FOREIGN KEY([StudentId])
REFERENCES [dbo].[Students] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ProfessorStudents] CHECK CONSTRAINT [FK_ProfessorStudents_Students_StudentId]
GO
USE [master]
GO
ALTER DATABASE [PostGrad] SET  READ_WRITE 
GO
