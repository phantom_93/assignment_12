﻿using System;
using System.Collections.Generic;
using System.Text;

namespace assignment_12.Models
{  
   
    public class Student
    {
        #region properties 
        
        public int Id { get; set; } // primary key(PK) for student class 
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public DateTime DOB { get; set; }

        public Gender Gender { get; set; }

        public string FullName
        {
            get
            {
                return $"{FirstName}  {LastName}"; 
            }
        }
        public ICollection<ProfessorStudent> HasProfessors { get; set; }
        #endregion

        public Student()
        {
            HasProfessors = new List<ProfessorStudent>();
        }

    }
}
