﻿using System;
using System.Collections.Generic;
using System.Text;

namespace assignment_12.Models
{
    public class ProfessorStudent
    {
        public int ProfessorId { get; set;}
        public int StudentId { get; set; }

        //Nav
        public Professor Professor { get; set; }
        public Student Student { get; set; }
    }
}
