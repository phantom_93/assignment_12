﻿using System;
using System.Collections.Generic;
using System.Text;

namespace assignment_12.Models
{
    public enum Gender
    {
        Male, 
        Female
    }
    public class Professor
    {
        #region properties 
        public int Id { get; set; }  //PK
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public DateTime DOB { get; set; }  // Date of birth

        public Gender Gender { get; set; }
        public ICollection<ProfessorStudent> HasStudents { get; set; }
        public string FullName
        {
            get
            {
                return $"{FirstName} {LastName}";
            }
        }

        #endregion

        public Professor()
       {
            HasStudents = new List<ProfessorStudent>(); 
       }
    }
}
