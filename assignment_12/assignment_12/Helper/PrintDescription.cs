﻿using System;
using System.Collections.Generic;
using System.Text;
using assignment_12.Models;

namespace assignment_12.Helper
{
    public class PrintDescription
    {
         public static string PrintStudent(Student student)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("\nStudent {\n");
            sb.Append($"\tFirst Name: {student.FirstName}\n"); // Student's full name
            sb.Append($"\tLast Name: {student.LastName}\n"); // Student's last name
            sb.Append($"\tDate of Birth: {student.DOB}\n"); // Student's Date of birth
            sb.Append($"\tGender: {student.Gender}\n"); // student's gender
            // Show student-professor relationship
            sb.Append("\tProfessors: {\n");
            foreach (ProfessorStudent professor in student.HasProfessors)
            {
                sb.Append($"\t\t{professor.Professor.FullName}\n");
            }
            sb.Append("}\n");

            return sb.ToString();
        }

        public static string PrintProfessor(Professor professor)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("\nProfessor {\n");
            sb.Append($"\tFull Name: {professor.FullName}\n"); // Professor's full name
            sb.Append($"\tFirst Name: {professor.FirstName}\n"); // Professor's full name
            sb.Append($"\tLast Name: {professor.LastName}\n"); // Professor's last name
            sb.Append($"\tGender: {professor.Gender}\n");  // Professor's gender
            // Show professor-student relationship 
            sb.Append("\tStudents: {\n");
            foreach (ProfessorStudent student in professor.HasStudents)
            {
                sb.Append($"\t\t{student.Student.FullName}\n");
            }
            sb.Append("}\n");

            return sb.ToString();
        }

    }
}
