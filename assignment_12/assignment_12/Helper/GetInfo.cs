﻿using assignment_12.SQL_Operations;
using assignment_12.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace assignment_12.Helper
{
    public class GetInfo
    {
        #region UserInput method 
        public static bool UserInput()
        {
            Student student = new Student();
            Professor professor = new Professor();
            bool runApp = true;
            while (runApp)
            {
                Console.WriteLine("Avaliable options in this program are: ");
                Console.WriteLine("\n0  - View a professor:" +
                                  "\n1  - View a student:" +
                                  "\n2  - View all professors:" +
                                  "\n3  - View all students:" +
                                  "\n4  - Add a student:" +
                                  "\n5  - Add a professor" +
                                  "\n6  - Add a relationship" +
                                  "\n7  - Remove a student" +
                                  "\n8  - Remove a professor" +
                                  "\n9  - Remove a relationship" + 
                                  "\n10 - Update professor" + 
                                  "\n11 - Update student" + 
                                  "\n12 - Export to json string" + 
                                  "\n13 - Quit");
                int userChoice = Convert.ToInt32(Console.ReadLine());
                switch (userChoice)
                {
                    case 0:
                        ViewaProfessor();
                        break;

                    case 1:
                        ViewaStudent(); 
                        break;

                    case 2:
                        ViewAllProfessors(); 
                        break;

                    case 3:
                        ViewAllStudents();
                        break;

                    case 4:
                        AddaStudent();
                        break;
                    case 5:
                        AddaProfessor();
                        break;

                    case 6:
                        AddaRelationship();
                        break;

                    case 7:
                        DeleteAStudent(); 
                        break;
                    case 8:
                        DeleteAProfessor();
                        break;
                    case 9:
                        RemoveaRelationship();
                        break;
                    case 10:
                        UpdateAProfessor();
                        break;
                    case 11:
                        UpdateAStudent();
                        break;
                    case 12:
                        WriteToJson(); 
                        break;
                    case 13:
                    default:
                        runApp = false;
                        break;
                }

            }
            return runApp; 
        }
        #endregion

        #region ViewaProfessor Method 
        public static void ViewaProfessor()
        {
            Console.WriteLine("Enter a professor's first name or last name you want to retrieve");
            string nameProfessor = Console.ReadLine().ToLower();
            IEnumerable<Professor> professors = Crud.GetAllProfessors();
            Professor prof = professors.Where(p => p.FirstName.ToLower().Equals(nameProfessor) || p.LastName.ToLower().Equals(nameProfessor)).Single();
            string profInfo = PrintDescription.PrintProfessor(prof);
            Console.WriteLine(profInfo);
        }
        #endregion


        #region ViewaStudent Method 
        public static void ViewaStudent()
        {
            Console.WriteLine("Enter a student's first name or last name you want to retrieve");
            string nameStudent = Console.ReadLine().ToLower();
            IEnumerable<Student> students = Crud.GetAllStudents();
            Student stud = students.Where(p => p.FirstName.ToLower().Equals(nameStudent) || p.LastName.ToLower().Equals(nameStudent)).Single();
            string studInfo = PrintDescription.PrintStudent(stud);
            Console.WriteLine(studInfo);
        }
        #endregion

        #region ViewAllProfesors Method 
        public static void ViewAllProfessors()
        {
            Console.WriteLine("Professors that are avaliable so far in the database are: ");
            IEnumerable<Professor> profList = Crud.GetAllProfessors();
            foreach (Professor professor in profList)
            {
                Console.WriteLine(PrintDescription.PrintProfessor(professor));
            }
        }
        #endregion

        #region ViewAllStudents Method 
        public static void ViewAllStudents()
        {
            Console.WriteLine("Students that are avaliable so far in the database are: ");
            IEnumerable<Student> studList = Crud.GetAllStudents();
            foreach (Student student in studList)
            {
                Console.WriteLine(PrintDescription.PrintStudent(student));
            }
        }
        #endregion


        #region AddaStudent Method 
        public static void AddaStudent()
        {
            try
            {
                Console.WriteLine("A student of your choice will be created");
                Console.WriteLine("\nEnter a First Name");
                string sFirstName = Console.ReadLine().ToLower();
                Console.WriteLine("\nEnter a Last Name");
                string sLastSName = Console.ReadLine().ToLower();
                Console.WriteLine("Enter the year of birth (yyyy): ");
                int year = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Enter the year of month (mm): ");
                int month = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Enter the year of birth (dd): ");
                int day = Convert.ToInt32(Console.ReadLine());
                DateTime dob = new DateTime(year, month, day);
                Console.Write("Select gender (Male or Female): ");
                Gender gender = (Gender)Enum.Parse(typeof(Gender), Console.ReadLine());
                Crud.AddStudent(sFirstName, sLastSName, dob,  gender); 
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        #endregion


        #region AddaProfessor Method 
        public static void AddaProfessor()
        {
            try
            {
                Console.WriteLine("A professor of your choice will be created");
                Console.WriteLine("\nEnter a First Name");
                string pFirstName = Console.ReadLine().ToLower();
                Console.WriteLine("\nEnter a Last Name");
                string pLastSName = Console.ReadLine().ToLower();
                Console.WriteLine("Enter the year of birth (yyyy): ");
                int year = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Enter the year of month (mm): ");
                int month = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Enter the year of birth (dd): ");
                int day = Convert.ToInt32(Console.ReadLine());
                DateTime dob = new DateTime(year, month, day);
                Console.Write("Select gender (Male or Female): ");
                Gender gender = (Gender)Enum.Parse(typeof(Gender), Console.ReadLine());
                Crud.AddProfessor(pFirstName, pLastSName, dob, gender);
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        #endregion


        #region AddaRelationship Method 
        public static void AddaRelationship()
        {
            try
            {
                Console.WriteLine("Enter an Id for a student you want to link");
                int studentId, professorId; 
                studentId = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Enter an Id for a professor you want to link");
                professorId = Convert.ToInt32(Console.ReadLine());
                Crud.AddRelationship(studentId, professorId); 
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }


        }
        #endregion



        #region RemoveaRelationship Method 
        public static void RemoveaRelationship()
        {
            try
            {
                Console.WriteLine("Enter an Id for a student you want to un-link");
                int studentId, professorId;
                studentId = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Enter an Id for a professor you want to un-link");
                professorId = Convert.ToInt32(Console.ReadLine());
                Crud.RemoveRelationship(studentId, professorId);
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }


        }
        #endregion




        #region DeleteAStudent method 
        public static void DeleteAStudent()
        {
            try
            {
                Console.WriteLine("Enter a student Id that you want to delete");
                int studId = Convert.ToInt32(Console.ReadLine());
                Student student = Crud.GetStudent(studId);
                Crud.RemoveStudent(student);
            }

            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        #endregion




        #region DeleteAProfessor method 
        public static void DeleteAProfessor()
        {
            try
            {
                Console.WriteLine("Enter a professor Id that you want to delete");
                int proffId = Convert.ToInt32(Console.ReadLine());
                Professor professor = Crud.GetProfessor(proffId);
                Crud.RemoveProfessor(professor);
            }

            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }


        }

        #endregion

        #region UpdateAProfessor method 
        public static void UpdateAProfessor()
        {
            try
            {
                Console.WriteLine("Enter a Professor id that you want to update");
                int proffId = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Enter the first name of the professor you want to alter (Leave empty to leave unchanged)");
                string firstName = Console.ReadLine();

                Console.WriteLine("Enter the last name of the professor you want to alter (Leave empty to leave unchanged)");
                string lastName = Console.ReadLine();

                Professor professor = Crud.GetProfessor(proffId);

                if (firstName != string.Empty)
                    professor.FirstName = firstName;
                if (lastName != string.Empty)
                    professor.LastName = lastName;

                Crud.UpdateProfessor(professor); 
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }
        #endregion


        #region UpdateAStudent Method 
        public static void UpdateAStudent()
        {
            try
            {
                Console.WriteLine("Enter a student id that you want to update");
                int proffId = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Enter the first name of the student you want to alter (Leave empty to leave unchanged)");
                string firstName = Console.ReadLine();

                Console.WriteLine("Enter the last name of the student you want to alter (Leave empty to leave unchanged)");
                string lastName = Console.ReadLine();

                Student student = Crud.GetStudent(proffId);

                if (firstName != string.Empty)
                    student.FirstName = firstName;
                if (lastName != string.Empty)
                    student.LastName = lastName;

                Crud.UpdateStudent(student);
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        #endregion



        #region writeToJson Method 
        public static void WriteToJson()
        {
            try
            {
                object[] data = new object[2];
                data[0] = Crud.GetAllStudents();
                data[1] = Crud.GetAllProfessors();

                Serializer.ExportDbToJson("exported.json", data);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        #endregion
    }
}
