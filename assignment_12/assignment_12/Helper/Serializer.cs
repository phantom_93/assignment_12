﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Newtonsoft.Json;

namespace assignment_12.Helper
{
    public class Serializer
    {
        public static void ExportDbToJson(string file, object obj)
        {
            string jsonString = JsonConvert.SerializeObject(obj, Formatting.Indented, 
                new JsonSerializerSettings {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });
            File.WriteAllText(file, jsonString); 

        }

    }
}
