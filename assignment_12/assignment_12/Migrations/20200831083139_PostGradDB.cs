﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace assignment_12.Migrations
{
    public partial class PostGradDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Professors",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    DOB = table.Column<DateTime>(nullable: false),
                    Gender = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Professors", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Students",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    DOB = table.Column<DateTime>(nullable: false),
                    Gender = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Students", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ProfessorStudents",
                columns: table => new
                {
                    ProfessorId = table.Column<int>(nullable: false),
                    StudentId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProfessorStudents", x => new { x.ProfessorId, x.StudentId });
                    table.ForeignKey(
                        name: "FK_ProfessorStudents_Professors_ProfessorId",
                        column: x => x.ProfessorId,
                        principalTable: "Professors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProfessorStudents_Students_StudentId",
                        column: x => x.StudentId,
                        principalTable: "Students",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Professors",
                columns: new[] { "Id", "DOB", "FirstName", "Gender", "LastName" },
                values: new object[,]
                {
                    { 1, new DateTime(1992, 3, 27, 0, 0, 0, 0, DateTimeKind.Unspecified), "Nicholas", 0, "Lennox" },
                    { 2, new DateTime(1992, 3, 27, 0, 0, 0, 0, DateTimeKind.Unspecified), "Dewald", 0, "Els" }
                });

            migrationBuilder.InsertData(
                table: "Students",
                columns: new[] { "Id", "DOB", "FirstName", "Gender", "LastName" },
                values: new object[,]
                {
                    { 1, new DateTime(1993, 3, 27, 0, 0, 0, 0, DateTimeKind.Unspecified), "Anna", 1, "Banana" },
                    { 2, new DateTime(1992, 3, 27, 0, 0, 0, 0, DateTimeKind.Unspecified), "Andre", 0, "Ottosen" },
                    { 3, new DateTime(1996, 12, 27, 0, 0, 0, 0, DateTimeKind.Unspecified), "Mathias", 0, "Brentsen" },
                    { 4, new DateTime(1996, 3, 27, 0, 0, 0, 0, DateTimeKind.Unspecified), "Nicolas", 0, "Anderson" },
                    { 5, new DateTime(1996, 3, 27, 0, 0, 0, 0, DateTimeKind.Unspecified), "Katharin", 1, "Potato" },
                    { 6, new DateTime(1996, 3, 27, 0, 0, 0, 0, DateTimeKind.Unspecified), "Natalaya", 1, "Stina" }
                });

            migrationBuilder.InsertData(
                table: "ProfessorStudents",
                columns: new[] { "ProfessorId", "StudentId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 1, 2 },
                    { 1, 3 },
                    { 2, 4 },
                    { 2, 5 },
                    { 2, 6 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_ProfessorStudents_StudentId",
                table: "ProfessorStudents",
                column: "StudentId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ProfessorStudents");

            migrationBuilder.DropTable(
                name: "Professors");

            migrationBuilder.DropTable(
                name: "Students");
        }
    }
}
