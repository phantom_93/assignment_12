﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using assignment_12.Models;
using Microsoft.EntityFrameworkCore;

namespace assignment_12
{
    class PostGradContext : DbContext
    {
        #region properties 
        public DbSet<Student> Students { get; set; }
        public DbSet<Professor> Professors { get; set; }

        public DbSet<ProfessorStudent> ProfessorStudents { get; set; }
        #endregion


        #region OnConfiguring Method
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source= PC7266\\SQLEXPRESS; Initial Catalog=PostGrad; Integrated Security=True;");
        }
        #endregion

        #region OnModelCreating Method 
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Composite key
            modelBuilder.Entity<ProfessorStudent>().HasKey(sp => new { sp.ProfessorId, sp.StudentId });

            // seed some student Data  
            modelBuilder.Entity<Student>().HasData(
                new Student()
                {
                    Id = 1,
                    FirstName = "Anna",
                    LastName = "Banana",
                    DOB = new DateTime(1993, 03, 27),
                    Gender = Gender.Female
                }
            );


            modelBuilder.Entity<Student>().HasData(
             new Student()
             {
                 Id = 2,
                 FirstName = "Andre",
                 LastName = "Ottosen",
                 DOB = new DateTime(1992, 03, 27),
                 Gender = Gender.Male
             }
         );


            modelBuilder.Entity<Student>().HasData(
              new Student()
              {
                  Id = 3,
                  FirstName = "Mathias",
                  LastName = "Brentsen",
                  DOB = new DateTime(1996, 12, 27),
                  Gender = Gender.Male
              }
          );



            modelBuilder.Entity<Student>().HasData(
              new Student()
              {
                  Id = 4,
                  FirstName = "Nicolas",
                  LastName = "Anderson",
                  DOB = new DateTime(1996, 03, 27),
                  Gender = Gender.Male
              }
          );


            modelBuilder.Entity<Student>().HasData(
             new Student()
             {
                 Id = 5,
                 FirstName = "Katharin",
                 LastName = "Potato",
                 DOB = new DateTime(1996, 03, 27),
                 Gender = Gender.Female
             });

              modelBuilder.Entity<Student>().HasData(
             new Student()
             {
                 Id = 6,
                 FirstName = "Natalaya",
                 LastName = "Stina",
                 DOB = new DateTime(1996, 03, 27),
                 Gender = Gender.Female
             }
         );

            // seed some professor Data 
            modelBuilder.Entity<Professor>().HasData(
            new Professor()
            {
                Id = 1,
                FirstName = "Nicholas",
                LastName = "Lennox",
                DOB = new DateTime(1992, 03, 27),
                Gender = Gender.Male
            }
        );



            modelBuilder.Entity<Professor>().HasData(
           new Professor()
           {
               Id = 2,
               FirstName = "Dewald",
               LastName = "Els",
               DOB = new DateTime(1992, 03, 27),
               Gender = Gender.Male
           }
         );
            // seeding Professor-student relationship
            for (int i = 0; i < 3; i++)
            {
                modelBuilder.Entity<ProfessorStudent>().HasData(
                new ProfessorStudent()
                {
                    ProfessorId = 1,
                    StudentId = (i + 1)
                }
                );
            }
            for (int i = 3; i < 6; i++)
            {
                modelBuilder.Entity<ProfessorStudent>().HasData(
                new ProfessorStudent()
                {
                    ProfessorId = 2,
                    StudentId = (i + 1)
                }
                );
            }

        }
        #endregion
    }
}
