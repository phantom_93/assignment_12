﻿using assignment_12.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.PortableExecutable;
using System.Text;

namespace assignment_12.SQL_Operations
{
    public class Crud
    {
        /* get a student/ professor
         get all students/ professors
         add a student/ professor 
         remove a student/ professor 
         update
         add a relationship
         remove a relationship
         */
        
        
        #region GetProfessor method 
        /*get a professor from the database */
        public static Professor GetProfessor(int id)
        {
            Professor professor = null;
            try
            {
                using (var data = new PostGradContext())
                {
                    // get a professor from database 
                    professor = data.Professors.Where(s => s.Id == id).SingleOrDefault();
                };

            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return professor;
        }

        #endregion


        #region GetStudent method 
        /*get a student from database */
        public static Student GetStudent(int id)
        {
            Student student = null;
            try
            {
                using (var data = new PostGradContext())
                {
                    // query for  getting a  student from database 
                    student = data.Students.Where(s => s.Id == id).SingleOrDefault();
                };

            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return student;
        }
        #endregion


        #region GetAllProfessors Method
        public static IEnumerable<Professor> GetAllProfessors()
        {
            IEnumerable<Professor> professors = null;
            try
            {
                using (var data = new PostGradContext())
                {
                    // query for  getting many students from database 
                    professors = data.Professors.Include(s => s.HasStudents).ThenInclude(s => s.Student).ToList();
                };

            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return professors; 
        }


        #endregion


        #region GetAllStudents Method
        public static IEnumerable<Student> GetAllStudents()
        {
            IEnumerable<Student> students = null;
            try
            {
                using (var data = new PostGradContext())
                {
                    // query for  getting many students from database 
                    students = data.Students.Include(s => s.HasProfessors).ThenInclude(s => s.Professor).ToList();
                };

            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return students; 
        }

        #endregion


       #region AddStudent method 
        /*this function will add a student to the database*/
        public static void AddStudent(string firstName, string lastName, DateTime dob, Gender gender)
        {
            try
            {
                using (var data = new PostGradContext())
                {
                    Student student = new Student() {FirstName = firstName, LastName = lastName, DOB = dob, Gender = gender}; 
                    // get a professor from database 
                    data.Students.Add(student);
                    Console.WriteLine(student.FirstName + ", " + student.LastName + ", " + student.DOB + ", " + student.Gender + ", saved");
                    data.SaveChanges(); 
                };

            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        #endregion


        #region AddProfessor method 
        /*this function will add a professor to a database*/
        public static void AddProfessor(string firstName, string lastName, DateTime dob, Gender gender)
        {
            try
            {
                using (var data = new PostGradContext())
                {
                    // get a professor from database 
                    Professor professor = new Professor() {FirstName = firstName, LastName = lastName, DOB = dob, Gender = gender};
                    data.Professors.Add(professor);
                    Console.WriteLine(professor.FirstName + ", " + professor.LastName + ", " + professor.DOB + ", " + professor.Gender + ", saved");
                    data.SaveChanges();
                };

            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        #endregion


        #region AddRelationship method 
        public static void AddRelationship(int studentId, int professorId)
        {
            try
            {
               using (var data = new PostGradContext())
                {
                    ProfessorStudent professorStudent = new ProfessorStudent() { ProfessorId = professorId, StudentId = studentId };
                    data.ProfessorStudents.Add(professorStudent); 
                    Console.WriteLine(professorId + ", "+ studentId + ", saved");
                    data.SaveChanges();
                }    

            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        #endregion



        #region RemoveRelationship method 
        /*remove a relationship*/
        public static void RemoveRelationship(int studentId, int professorID)
        {
            try
            {
                using (var data = new PostGradContext())
                {
                    // create a query that gets both 
                    IEnumerable<ProfessorStudent> relations = data.ProfessorStudents.Where(r => r.ProfessorId == professorID
                                                                                            && r.StudentId == studentId);
                    data.ProfessorStudents.RemoveRange(relations);
                    //data.ProfessorStudents.Remove()
                    data.SaveChanges();
                }

            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        #endregion


        public static void RemoveRelationship(Student student, Professor professor)
        {
            try
            {
                using (var data = new PostGradContext())
                {
                    // create a query that gets both 
                    IEnumerable<ProfessorStudent> relations = data.ProfessorStudents.Where(r => r.ProfessorId == professor.Id
                                                                                            && r.StudentId == student.Id);
                    data.ProfessorStudents.RemoveRange(relations);
                    //data.ProfessorStudents.Remove()
                    data.SaveChanges();
                }

            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }




        #region RemoveProfessor method 
        /*remove a professor from a database and his/her relationship with his/her student*/
        public static void RemoveProfessor(Professor professor)
        {

            try
            {
                using (var data = new PostGradContext())
                {
                    foreach (ProfessorStudent relation in professor.HasStudents)
                    {
                        RemoveRelationship(relation.Student, professor);
                    }
                    // get a professor from database 
                    data.Professors.Remove(professor);
                    data.SaveChanges();
                };

            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }
        #endregion




        #region RemoveStudent method 
        /*remove a student from database and also his/ her relationship with his/her students */
        public static void RemoveStudent(Student student)
        {
            try
            {
                using (var data = new PostGradContext())
                {
                    
                    foreach(ProfessorStudent relation in student.HasProfessors)
                    {
                       RemoveRelationship(student, relation.Professor);
                    }
                    // get a professor from database 
                    data.Students.Remove(student);
                    data.SaveChanges();
                };

            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }
        #endregion



        #region UpdateStudent method 
        /*update First Name 
         update  Last  Name 
         update  DOB = date of birth 
         update Gender*/
        public static void UpdateStudent(params Student[] students)
        {
            try
            {
                using (var data = new PostGradContext())
                {
                    foreach (Student student in students)
                    {
                        Student _student = data.Students.Find(student.Id);

                        _student.FirstName = student.FirstName;
                        _student.LastName = student.LastName;
                        _student.DOB = student.DOB;
                        _student.Gender = student.Gender;
                    }

                    data.SaveChanges();
                }
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        #endregion


        #region UpdateProfessor method 
        /*update First Name 
         update  Last  Name 
         update  DOB = date of birth 
         update Gender*/
        public static void UpdateProfessor(params Professor[] professors)
        {
            try
            {
                using (var data = new PostGradContext())
                {
                    foreach (Professor professor in professors)
                    {
                        Professor _professor = data.Professors.Find(professor.Id);

                        _professor.FirstName = professor.FirstName;
                        _professor.LastName = professor.LastName;
                        _professor.DOB = professor.DOB;
                        _professor.Gender = professor.Gender;
                    }

                    data.SaveChanges();
                }
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        #endregion


    }
}
