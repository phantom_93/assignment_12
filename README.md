Assignment 12 - PostGraf EF
Type:    Windows Form or Console Application

•Allow a university post grad administrator to assign Supervisors (Professors) to Students to supervise them during their postgrad research degrees.

Requirements:

•Use a code first workflow to create the initial migrations and the corresponding database using entity framework (DBContext)
•Can either be a one to one relationship (A professor can only supervise one student) OR a one to many (A professor can supervise many students and a student can only have one professor)
•Navigation properties
•Must demonstrate adding these entities to the database
•This means just migrations, not adding data.
•Purpose: Learn Entity Framework: Migrations

Weight: Necessary

Pt2 Additions:
Type:     Windows Form or Console Application

Upgrade your previous solution to include a one to many, many to many and one to one relationship of your choice. (If you included a one to one in the first version then add a one to many and vice versa)

Requirements:    Create the relevant methods to allow for CRUD operations – it is up to you which operations are available to the user depending on how you designed your relational model.

There must be at least 2 entities where CRUD operations are performed.

Add comments in the model classes explaining the navigation properties and FKs (i.e ProfId is a FK reference to the Professor entity, and the Professor object is the navigation property from student (dependent class) to professor (principle class)). The comments will be marked as part of functionality as it is important for you to understand what the conventions mean. 

Purpose: Learn Entity Framework: Relationships

Pt3 Additions

Add a method into your PGManager project to allow for the current DBSet of Supervisors/Professors to be serialized into JSON. 
You can either write the JSON to a text file within your debug folder or add a component to your UI to display the JSON text.

Purpose: Learn JSON usage within .NET.


-----------------------------------------------------------------------------------------------
Through microsoft's "Nuget packet manager", the following packages are installed. 

**Microsoft.EntityFrameworkCore.SqlServer**

**Microsoft.EntityFrameworkCore.Tools**

**NewtonsoftJson**


This program is console based application that demonstrates many-to-many relationships between students and professors using EF = Entity Framwork Code first approach. That is a student can have many professors and a professor can have many students at school of Noroff for instance. In order to build a database using framework through "packet manager console"

**add-migration PostGrad**

the next command will be

"update-database"


Technical concepts that covered in this application are CRUD operations, Serializing and File Writing. CRUD shortly stands for create, read, update and delete. In order to create a professor or student object in the mysql server database, add functions are used. In addition to view either a profess/ a list of professor or to view a student/ a list of students, getAll or getA functions are used. It is also possible to add/remove a relationship between a student and a professor using their IDs as arguments. 
Moreover, it is possible to delete/update both professor and student using their respective functions and they are able to print out in a json file format. 

In order to make it user-friendly, the program is implemented in a way that the user will get a chance to choose with a list of choices. These choices are as follow.


0. View a professor 
1. View a atudent
2. View all professors 
3. View all students 
4. Add a student
5. Add a professor 
6. Add a relationship 
7. Remove a student
8. Remove a professor 
9. Remove a relationship. 
10. Update a professor 
11. Update a student
12. Export to json file.
13. Quit 
